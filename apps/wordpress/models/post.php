<?php

class PostModel extends \Steam\Model
{
    protected static function _retrieve(\Steam\Model\Query &$query, \Steam\Model\Response &$response)
    {
        $posts = query_posts((string) $query->parameters);
        
        $response->add_items($posts);
        $response->status = 200;
        
        wp_reset_query();
    }
}

?>
