<?php
/*
Plugin Name: WP-Steam
*/

include 'functions.php';

include 'libraries/Steam.php';

function steam_init()
{
    ob_start();

    Steam::load_config();

    Steam::initialize();


    add_filter('rewrite_rules_array', 'steam_rewrites');
    
    add_filter('query_vars', 'steam_query_vars');
    
    add_action('template_redirect', 'steam_template_redirect');
}

function steam_query_vars($wp_vars)
{
    $wp_vars[] = 'steam_action';
    $wp_vars[] = 'steam_model';
    $wp_vars[] = 'steam_view';
    
    return $wp_vars;
}

function steam_template_redirect()
{
    $action = get_query_var('steam_action');
    $model  = get_query_var('steam_model');
    $view   = get_query_var('steam_view');
    
    if ($action)
    {
        $type = 'action';
        $resource = $action;
    }
    elseif ($model)
    {
        $type = 'model';
        $resource = $model;
    }
    elseif ($view)
    {
        $type = 'view';
        $resource = $view;
    }
    else
    {
        return;
    }
    
    Steam::$app           = 'wordpress';
    Steam::$resource_type = $type;
    Steam::$resource      = $resource;

    Steam::dispatch();

    exit;
}

function steam_rewrites($wp_rules)
{
    /*
    $base = get_option('json_api_base', 'api');
    
    if (empty($base))
    {
        return $wp_rules;
    }
    */
    
    $steam_rules = array(
        'actions/(.+)$' => 'index.php?steam_action=$matches[1]',
        'models/(.+)$'  => 'index.php?steam_model=$matches[1]',
        'views/(.+)$'   => 'index.php?steam_view=$matches[1]',
    );
    
    return array_merge($steam_rules, $wp_rules);
}

function json_api_activation()
{
    global $wp_rewrite;
    add_filter('rewrite_rules_array', 'steam_rewrites');
    $wp_rewrite->flush_rules();
}

function json_api_deactivation()
{
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}

add_action('init', 'steam_init');

register_activation_hook(Steam::path('wp-steam.php'), 'steam_activation');

register_deactivation_hook(Steam::path('wp-steam.php'), 'steam_deactivation');

?>
